package Bai_Tap;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;
import javax.swing.Timer;

public class Client_app extends Thread implements ActionListener
{
	public int id;
	public boolean running;
	public Random r = new Random();
	public Socket socket;
	public DataInputStream dis;
	public DataOutputStream dos;
	public Timer timer;
	public Client_app(int id)
	{
		this.id = id;
		running = true;
		try{
			timer = new Timer(1000, this);
			socket = new Socket("localhost", 3000);
			this.dis = new DataInputStream(socket.getInputStream());
			this.dos = new DataOutputStream(socket.getOutputStream());
			this.dos.writeUTF("Joint,"+this.id);
		}catch(IOException e){}
	}
	public synchronized void run()
	{
		try{
			System.out.println("thread id "+this.id+" is started!");
			timer.start();
			while(running)
			{			
				String msg = dis.readUTF();
				if (msg.equals("kill"))
				{
					running = false;
					System.out.println("thread id "+this.id+" is destroyed!");
					this.socket.close();
					timer.stop();
					break;
				}
				Thread.sleep(1000);
			}
		}
		catch(Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Client_app[] clients = new Client_app[200];
		for(int i = 0;i < 200;i++)
		{
			clients[i] = new Client_app(i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for(int i = 0;i < 200;i++)
		{
			clients[i].start();
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int des = r.nextInt(200);
		try {
			dos.writeUTF(id+","+des);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("destination is "+des);
	}
}

	