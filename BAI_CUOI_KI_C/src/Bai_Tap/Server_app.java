package Bai_Tap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Server_app {

	public ArrayList<ThreadedHandler> clients = new ArrayList<ThreadedHandler>();
	public Lock AccessLock = new ReentrantLock();
	public Server_app()
	{
		ServerSocket Server;
		Socket Connection;
		try {
			Server = new ServerSocket(3000);
			System.out.println("Server is listening");
			while (true) {
				Connection = Server.accept();
				System.out.println("Have Connection!");
				new ThreadedHandler(this,Connection).start();
			}
		}catch (IOException e) {
		System.err.println(e);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Server_app();
	}
	
	public class ThreadedHandler extends Thread
	{
		public int id;
		public Server_app sv;
		public Socket incoming;
		public DataInputStream dis;
		public DataOutputStream dos;
		public ThreadedHandler(Server_app sv, Socket incoming)
		{
			this.sv = sv;
			this.incoming = incoming;
			try{
				this.dis = new DataInputStream(incoming.getInputStream());
				this.dos = new DataOutputStream(incoming.getOutputStream());
			}catch(IOException e){}
		}
		public void run()
		{
			String msg = "";
			try {
				msg = dis.readUTF();
				System.out.println(msg);
				
				if(!msg.contains("Joint")) 
				{
					this.incoming.close();
					System.out.println("Incorect message");
				}
				else
				{
					id = Integer.parseInt(msg.split(",")[1]);
					this.sv.clients.add(this);
					System.out.println("thread id "+id+" is  running now");
				}
				while(true)
				{				
					boolean ElementExist = false;
					ThreadedHandler handler = null;
					msg = dis.readUTF();
					String[] array = msg.split(",");
					int desId = Integer.parseInt(array[1]);
					AccessLock.lock();
					for (ThreadedHandler iter: this.sv.clients) {
						if (desId == iter.id) {
							ElementExist = true;
							handler = iter;
							break;
						}
					}
					AccessLock.unlock();
					if (ElementExist)
					{
						handler.dos.writeUTF("kill");
						handler.incoming.close();
						this.sv.clients.remove(handler);
						ElementExist = false;
						System.out.println("Thread id "+ desId+" is killed");
					}
					else
					{
						System.out.println("Can't kill thread id "+ desId);
					}
					if (this.sv.clients.isEmpty())
					{
						System.out.println("All of thread is terminated!");
						return ;
					}
				}
			}
			catch (IOException e) {
				try {
					this.incoming.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

}
